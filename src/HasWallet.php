<?php

namespace Yeknava\SimpleWallet;

use Throwable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleWallet\Exceptions\InvalidAmountException;
use Yeknava\SimpleWallet\Exceptions\WalletNotFoundException;
use Yeknava\SimpleWallet\Exceptions\NotEnoughCreditException;
use Yeknava\SimpleWallet\Exceptions\RefundedTransactionException;

trait HasWallet
{

    protected static function walletBoot()
    {
        parent::boot();

        static::created(function ($model) {
            SimpleWallet::makeNew([], $model);
        });

        static::deleted(function ($model) {
            $model->wallets()->delete();
        });

        if (method_exists(__CLASS__, "restore")) {
            static::restored(function ($model) {
                $model->wallets()->restore();
            });
        }
    }

    public function wallets()
    {
        return $this->morphMany(SimpleWallet::class, 'holder');
    }

    public function balance(string $slug = null)
    {
        return $this->wallet($slug, false)->balance;
    }

    public function lockedBalance(string $slug = null)
    {
        return $this->wallet($slug, false)->locked_balance;
    }

    public function blocked()
    {
        return $this->lockedBalance();
    }

    public function createWallet(array $data, string $slug = null, string $currency = null)
    {
        return SimpleWallet::makeNew($data, $this, $slug, $currency);
    }

    public function wallet(string $slug = null, bool $lockForUpdate = true)
    {
        $wallet = $this->wallets();

        if (!empty($slug)) $wallet = $wallet->where('slug', $slug);
        else $wallet = $wallet->where('is_default', true);

        if ($lockForUpdate) {
            $wallet->lockForUpdate();
        }

        $wallet = $wallet->first();

        if (empty($wallet) && config('simple-wallet.create_wallet_on_fetch') === true) {
            $wallet = $this->createWallet([], $slug);

            if ($lockForUpdate) {
                $wallet = SimpleWallet::lockForUpdate()->where('id', $wallet->id)->first();
            }
        } elseif (empty($wallet) && !config('simple-wallet.create_wallet_on_fetch')) {
            throw new WalletNotFoundException();
        }

        return $wallet;
    }

    public function updateWallet(array $data, string $slug = null)
    {
        return $this->wallet($slug)->fill($data)->save();
    }

    public function deposit(
        float $amount,
        array $extra = null,
        SimpleWallet $wallet = null,
        SimpleWallet $system = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            $payment = new Payment(SimpleWallet::DEPOSIT);
            $payment->setPayee($wallet);
            $payment->setAmount($amount);

            if ($extra) $payment->setExtra($extra);

            if ($system) $payment->setPayer($system);

            $payment->pay();

            app('db')->commit();
            return $wallet;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function refund(
        SimpleWalletTransaction $transaction,
        array $extra = null,
        SimpleWallet $wallet = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            if (!empty($transaction->refunded_at)) {
                throw new RefundedTransactionException();
            }

            $payment = new Payment(SimpleWallet::REFUND);
            $payment->setPayee($wallet);
            $payment->setAmount($transaction->amount);

            if ($extra) $payment->setExtra($extra);
            if ($transaction->service) $payment->setService($transaction->service);
            if (!empty($payeeWallet = $transaction->payeeWallet)) {
                $payment->setPayer($payeeWallet);
            }
            $payment = $payment->pay();

            $transaction->refunded_at = Carbon::now();
            $extra = $transaction->extra;
            $extra['refund_transaction_id'] = $payment->id;
            $transaction->extra = $extra;
            $transaction->save();

            app('db')->commit();
            return $wallet;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function withdraw(
        float $amount,
        array $extra = null,
        SimpleWallet $wallet = null,
        SimpleWallet $system = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            $payment = new Payment(SimpleWallet::WITHDRAW);
            $payment->setPayer($wallet);
            $payment->setAmount($amount);

            if ($extra) $payment->setExtra($extra);

            if ($system) $payment->setPayee($system);

            $payment = $payment->pay();

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function forceWithdraw(
        float $amount,
        array $extra = null,
        SimpleWallet $wallet = null,
        SimpleWallet $system = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            $payment = new Payment(SimpleWallet::WITHDRAW);
            $payment->setForce(true);
            $payment->setPayer($wallet);
            $payment->setAmount($amount);

            if ($extra) $payment->setExtra($extra);

            if ($system) $payment->setPayer($system);

            $payment = $payment->pay();

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function transfer(
        float $amount,
        SimpleWallet $payeeWallet,
        SimpleWallet $wallet = null,
        array $extra = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            $payment = new Payment(SimpleWallet::TRANSFER);
            $payment->setPayer($wallet);
            $payment->setAmount($amount);
            $payment->setPayee($payeeWallet);

            if ($extra) $payment->setExtra($extra);

            $payment = $payment->pay();

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function forceTransfer(
        float $amount,
        SimpleWallet $payeeWallet,
        SimpleWallet $wallet = null,
        array $extra = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            $payment = new Payment(SimpleWallet::TRANSFER);
            $payment->setForce(true);
            $payment->setPayer($wallet);
            $payment->setAmount($amount);
            $payment->setPayee($payeeWallet);

            if ($extra) $payment->setExtra($extra);

            $payment = $payment->pay();

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function gift(
        float $amount,
        SimpleWallet $payerWallet = null,
        SimpleWallet $payeeWallet = null,
        array $extra = null
    ) {
        try {
            app('db')->beginTransaction();

            $payment = new Payment(SimpleWallet::GIFT);

            if (empty($payeeWallet)) {
                $payeeWallet = $this->wallet();
            }

            $payment->setPayee($payeeWallet);
            $payment->setAmount($amount);
            if ($extra) $payment->setExtra($extra);

            if (!empty($payerWallet)) {
                $payment->setPayer($payerWallet);
            }

            $payment = $payment->pay();

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function systemGift(
        float $amount,
        SimpleWallet $wallet = null,
        array $extra = null,
        SimpleWallet $system = null
    ) {
        try {
            app('db')->beginTransaction();

            if (empty($wallet)) {
                $wallet = $this->wallet();
            } else {
                $this->wallet($wallet->slug);
            }

            $payment = new Payment(SimpleWallet::SYSTEM_GIFT);
            $payment->setPayee($wallet);
            $payment->setAmount($amount);

            if (!empty($system)) {
                $payment->setPayer($system);
            }

            if ($extra) $payment->setExtra($extra);

            $payment = $payment->pay();

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function purchase(
        float $amount,
        Model $service = null,
        SimpleWallet $payeeWallet = null,
        SimpleWallet $payerWallet = null,
        array $extra = null,
        float $cashbackAmount = 0
    ) {
        try {
            app('db')->beginTransaction();
            if (empty($payerWallet)) {
                $payerWallet = $this->wallet();
            }

            $payment = new Payment(SimpleWallet::PURCHASE);
            $payment->setPayer($payerWallet);
            $payment->setAmount($amount);

            if ($payeeWallet) {
                $payment->setPayee($payeeWallet);
            }

            if ($service) $payment->setService($service);
            if ($extra) $payment->setExtra($extra);

            $payment = $payment->pay();

            if ($cashbackAmount) {
                $this->cashback($cashbackAmount, $payerWallet, $service, $payeeWallet);
            }

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function forcePurchase(
        float $amount,
        Model $service = null,
        SimpleWallet $payeeWallet = null,
        SimpleWallet $payerWallet = null,
        array $extra = null,
        float $cashbackAmount = 0
    ) {
        try {
            app('db')->beginTransaction();
            if (empty($payerWallet)) {
                $payerWallet = $this->wallet();
            }

            $payment = new Payment(SimpleWallet::PURCHASE);
            $payment->setForce(true);
            $payment->setPayer($payerWallet);
            $payment->setAmount($amount);

            if (!empty($payeeWallet)) {
                $payment->setPayee($payeeWallet);
            }

            if (!empty($service)) $payment->setService($service);

            if ($extra) $payment->setExtra($extra);

            $payment = $payment->pay();

            if ($cashbackAmount > 0) {
                $this->cashback(
                    $cashbackAmount,
                    $payerWallet,
                    $service,
                    $payeeWallet,
                    $extra
                );
            }

            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function cashback(
        float $amount,
        SimpleWallet $payeeWallet,
        Model $service = null,
        SimpleWallet $payerWallet = null,
        array $extra = null
    ) {
        try {
            app('db')->beginTransaction();

            $payment = new Payment(SimpleWallet::CASH_BACK);
            $payment->setAmount($amount);
            $payment->setPayee($payeeWallet);

            if (!empty($payerWallet)) {
                $payment->setPayer($payerWallet);
            }

            if ($service) $payment->setService($service);
            if ($extra) $payment->setExtra($extra);

            $payment = $payment->pay();
            app('db')->commit();
            return $payment;
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function lockBalance(float $amount = null, string $slug = null)
    {
        try {
            app('db')->beginTransaction();

            $wallet = $this->wallet($slug);

            if ($amount && $amount > $wallet->balance) {
                throw new NotEnoughCreditException();
            }

            $wallet->locked_balance = (float) $wallet->locked_balance +
                $amount ?? (float) $wallet->balance;
            $wallet->balance = (float) $wallet->balance -
                $amount ?? (float) $wallet->balance;
            $wallet->save();

            app('db')->commit();
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function releaseBalance(float $amount = null, string $slug = null)
    {
        try {
            app('db')->beginTransaction();

            $wallet = $this->wallet($slug);

            if ($amount && $amount > $wallet->locked_balance) {
                throw new InvalidAmountException();
            }

            $wallet->balance = (float) $wallet->balance +
                ($amount ?? (float) $wallet->locked_balance);
            $wallet->locked_balance = (float) $wallet->locked_balance -
                ($amount ?? (float) $wallet->locked_balance);
            $wallet->save();

            app('db')->commit();
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function lockWallet(string $slug = null)
    {
        try {
            app('db')->beginTransaction();

            $wallet = $this->wallet($slug);
            $wallet->locked = 1;
            $wallet->locked_at = date('Y-m-d H:i:s');
            $wallet->save();

            app('db')->commit();
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function fullLockWallet(string $slug = null)
    {
        try {
            app('db')->beginTransaction();

            $wallet = $this->wallet($slug);
            $wallet->locked = 2;
            $wallet->locked_at = date('Y-m-d H:i:s');
            $wallet->save();

            app('db')->commit();
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function releaseWallet(string $slug = null)
    {
        try {
            app('db')->beginTransaction();

            $wallet = $this->wallet($slug);
            $wallet->locked = 0;
            $wallet->locked_at = null;
            $wallet->save();

            app('db')->commit();
        } catch (Throwable $error) {
            app('db')->rollBack();
            throw $error;
        }
    }

    public function transactions(
        string $type = null,
        SimpleWallet $wallet = null
    ) {
        if (empty($wallet)) {
            $wallet = $this->wallet(null, false);
        }

        $query = SimpleWalletTransaction::where('payer_wallet_id', $wallet->id)
            ->orWhere('payee_wallet_id', $wallet->id);

        if (!empty($type)) {
            $query = $query->where('type', $type);
        }

        return $query->orderBy('id', 'desc');
    }

    public function servicePurchases(
        Model $service,
        SimpleWallet $wallet = null
    ) {
        if (empty($wallet)) {
            $wallet = $this->wallet(null, false);
        }

        return SimpleWalletTransaction::where('payer_wallet_id', $wallet->id)
            ->where('service_id', $service->id)
            ->where('service_type', $service->getMorphClass())
            ->orderBy('id', 'desc');
    }

    public function sumServicePurchases(...$args): int
    {
        return $this->servicePurchases(...$args)->sum('amount');
    }

    public function hasService(...$args): bool
    {
        return $this->sumServicePurchases(...$args) ? true : false;
    }
}
