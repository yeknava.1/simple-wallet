<?php

namespace Yeknava\SimpleWallet\Samples;

use Yeknava\SimpleWallet\RateServiceInterface;

class UsdRateServiceSample implements RateServiceInterface
{
    public function convert(float $amount, string $currency): float
    {
        return $amount;
    }
}
