<?php

namespace Yeknava\SimpleWallet;

use Illuminate\Support\ServiceProvider;

class SimpleWalletServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            dirname(__DIR__, 1) . '/config/simple-wallet.php' =>
            $this->app->configPath() . '/simple-wallet.php',

            dirname(__DIR__, 1) . '/migrations/' =>
            database_path('migrations'),
        ]);
    }
}
