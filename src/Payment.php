<?php

namespace Yeknava\SimpleWallet;

use Throwable;
use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleWallet\Exceptions\LockedWalletException;
use Yeknava\SimpleWallet\Exceptions\PayeeNotFoundException;
use Yeknava\SimpleWallet\Exceptions\PayerNotFoundException;
use Yeknava\SimpleWallet\Exceptions\NotEnoughCreditException;
use Yeknava\SimpleWallet\Exceptions\InvalidTransactionException;

class Payment
{
    public $payer;
    public $payee;
    public $amount;
    public $currency;
    public $extra;
    public $type;
    public $force;
    public $service;

    public function __construct(string $type)
    {
        $this->extra = [];
        $this->type = $type;
        $this->force = false;
    }

    public function setPayer(SimpleWallet $payer): self
    {
        $this->payer = $payer;

        return $this;
    }

    public function setPayee(SimpleWallet $payee): self
    {
        $this->payee = $payee;

        return $this;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function setForce(string $force): self
    {
        $this->force = $force;

        return $this;
    }

    public function setService(Model $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function setExtra(array $extra): self
    {
        $this->extra = $extra;
        if (!empty($this->extra['service'])) {
            $this->setService($this->extra['service']);
            unset($this->extra['service']);
        }

        return $this;
    }

    public function pay(): SimpleWalletTransaction
    {
        try {
            app('db')->beginTransaction();
            if ($this->payer && $this->payee && $this->payer->id === $this->payee->id) {
                throw new InvalidTransactionException("payer and payee can not be same wallet.");
            }
            if (!$this->payer && in_array($this->type, [
                SimpleWallet::TRANSFER,
                SimpleWallet::GIFT,
                SimpleWallet::PURCHASE,
                SimpleWallet::WITHDRAW
            ])) {
                throw new PayerNotFoundException();
            }

            if (!$this->payee && in_array($this->type, [
                SimpleWallet::TRANSFER,
                SimpleWallet::SYSTEM_GIFT,
                SimpleWallet::GIFT,
                SimpleWallet::CASH_BACK,
                SimpleWallet::DEPOSIT,
                SimpleWallet::REFUND
            ])) {
                throw new PayeeNotFoundException();
            }

            if ($this->payee && (int) $this->payee->locked === 2) {
                throw new LockedWalletException();
            }

            if (!$this->currency && $this->payee) {
                $this->currency = $this->payee->currency;
            } elseif (!$this->currency && !$this->payee) {
                $this->currency = config('simple-wallet.default_currency');
            }

            $fee = 0;

            if (!empty($this->extra['fee'])) {
                $fee = $this->extra['fee'];
                unset($this->extra['fee']);
            }

            if ($this->payer) {
                if ((int) $this->payer->locked >= 1) {
                    throw new LockedWalletException();
                }
                if ($this->payer->balance < $this->amount && $this->force === false) {
                    throw new NotEnoughCreditException();
                }
                $payerAmount = $this->amount + $fee;
                if ($this->payer->currency !== $this->currency) {
                    $payerAmount = $this->payer->exchange($this->amount, $this->currency);
                }
                $currentPayerBalance = $this->payer->balance;
                $newPayerBalance = $this->payer->balance - $payerAmount;
                $this->payer->balance -= $payerAmount;
                $this->payer->save();
            }

            $payment = new SimpleWalletTransaction([
                'amount' => $this->amount,
                'currency' => $this->currency,
                'extra' => $this->extra ?? [],
                'type' => $this->type,
                'fee' => $fee
            ]);

            if (!empty($this->payee)) {
                $payment->payeeWallet()->associate($this->payee);
            }
            if (!empty($this->payer)) {
                $payment->payerWallet()->associate($this->payer);
            }
            if (!empty($this->service)) {
                $payment->service()->associate($this->service);
            }

            $payment->save();

            if ($this->payee) {
                $payeeAmount = $this->amount;
                if ($this->payee->currency !== $this->currency) {
                    $payeeAmount = $this->payee->exchange($this->amount, $this->currency);
                }
                $newBalance = $this->payee->balance + $payeeAmount;
                $currentBalance = $this->payee->balance;
                $this->payee->balance = $newBalance;
                $this->payee->save();

                $payeeLog = new SimpleWalletLog([
                    'amount' => $payeeAmount,
                    'type' => SimpleWalletLog::TYPE_DEPOSIT,
                    'balance' => $currentBalance,
                    'new_balance' => $newBalance
                ]);
                $payeeLog->wallet()->associate($this->payee);
                $payeeLog->walletTransaction()->associate($payment);
                $payeeLog->save();
            }

            if (!empty($this->payer)) {
                $payerLog = new SimpleWalletLog([
                    'amount' => $payerAmount,
                    'type' => SimpleWalletLog::TYPE_WITHDRAW,
                    'balance' => $currentPayerBalance,
                    'new_balance' => $newPayerBalance
                ]);
                $payerLog->wallet()->associate($this->payer);
                $payerLog->walletTransaction()->associate($payment);
                $payerLog->save();
            }

            app('db')->commit();

            return $payment;
        } catch (Throwable $e) {
            app('db')->rollback();
            throw $e;
        }
    }
}
