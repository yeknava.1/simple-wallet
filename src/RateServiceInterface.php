<?php

namespace Yeknava\SimpleWallet;

interface RateServiceInterface {
    public function convert(float $amount, string $currency) : float;
}