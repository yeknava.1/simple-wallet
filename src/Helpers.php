<?php

namespace Yeknava\SimpleWallet;

use ReflectionClass;
use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleWallet\Exceptions\InvalidHolderException;
use Yeknava\SimpleWallet\Exceptions\InvalidServiceRateException;

class Helpers
{
    public function currencyRate(string $service, float $amount, string $currency): float
    {
        $service = new ReflectionClass($service);

        if (!$service->implementsInterface(RateServiceInterface::class)) {
            throw new InvalidServiceRateException("service must implements interface " . RateServiceInterface::class);
        }

        return $service->newInstance()->convert($amount, $currency);
    }

    public function getWallet($model, string $slug = null): SimpleWallet
    {
        if ($model instanceof SimpleWallet) return $model;
        elseif ($model instanceof Model) return $model->wallet($slug);

        throw new InvalidHolderException();
    }
}
