<?php

namespace Yeknava\SimpleWallet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SimpleWalletLog extends Model
{
    use SoftDeletes;

    const TYPE_DEPOSIT = 'deposit';
    const TYPE_WITHDRAW = 'withdraw';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-wallet.wallet_logs_table');
    }

    protected $fillable = [
        'amount', 'type', 'balance', 'new_balance'
    ];

    protected $casts = [];

    public function wallet()
    {
        return $this->belongsTo(SimpleWallet::class, 'wallet_id');
    }

    public function walletTransaction()
    {
        return $this->belongsTo(SimpleWalletTransaction::class, 'wallet_transaction_id');
    }
}
