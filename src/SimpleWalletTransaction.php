<?php

namespace Yeknava\SimpleWallet;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SimpleWalletTransaction extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-wallet.transactions_table');
    }

    protected $fillable = [
        'amount', 'fee', 'type',
        'currency', 'extra',
    ];

    protected $casts = [
        'extra' => 'array',
        'refunded_at' => 'datetime'
    ];

    public function payerWallet()
    {
        return $this->belongsTo(SimpleWallet::class, 'payer_wallet_id');
    }

    public function payeeWallet()
    {
        return $this->belongsTo(SimpleWallet::class, 'payee_wallet_id');
    }

    public function service()
    {
        return $this->morphTo();
    }

}
