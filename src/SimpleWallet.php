<?php

namespace Yeknava\SimpleWallet;

use Throwable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleWallet\Exceptions\InvalidCurrencyException;
use Yeknava\SimpleWallet\Exceptions\InvalidServiceRateException;

class SimpleWallet extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('simple-wallet.wallets_table');
    }

    const TRANSFER = 'transfer';
    const DEPOSIT = 'deposit';
    const WITHDRAW = 'withdraw';
    const GIFT = 'gift';
    const SYSTEM_GIFT = 'system_gift';
    const CASH_BACK = 'cash_back';
    const PURCHASE = 'purchase';
    const REFUND = 'refund';

    protected $fillable = [
        'slug', 'name', 'type', 'is_default'
    ];

    public function holder()
    {
        return $this->morphTo();
    }

    public function payeeTransactions()
    {
        return $this->hasMany(SimpleWalletTransaction::class, 'payee_wallet_id');
    }

    public function payerTransactions()
    {
        return $this->hasMany(SimpleWalletTransaction::class, 'payer_wallet_id');
    }

    public function logs()
    {
        return $this->hasMany(SimpleWalletLog::class, 'wallet_id');
    }

    public static function makeNew(
        array $data,
        Model $holder,
        string $slug = null,
        string $currency = null
    ): self {
        $wallet = new self($data);

        $wallet->balance = 0;
        $wallet->locked_balance = 0;
        $wallet->currency = $currency ?? config('simple-wallet.default_currency');
        if (empty($slug)) {
            $wallet->slug = config('simple-wallet.wallet_default_slug');
        } else {
            $wallet->slug = $slug;
        }
        if ($holder->wallets()->count()) {
            $wallet->is_default = false;
        }
        $holder->wallets()->save($wallet);

        return $wallet;
    }

    public function makeDefault()
    {
        try {
            app('db')->beginTransaction();
            $this->where('is_default', true)->update(['is_default' => false]);
            $this->fill(['is_default' => true])->save();
            app('db')->commit();

            return $this;
        } catch (Throwable $e) {
            app('db')->rollback();
            throw $e;
        }
    }

    public function exchange(float $amount, string $currency, RateServiceInterface $service = null): float
    {
        if (!in_array($currency, config('simple-wallet.currencies'))) {
            throw new InvalidCurrencyException();
        }

        $rate = config('simple-wallet.rates')[$this->currency];

        if (!empty($service)) {
            return $service->convert($amount, $currency);
        } elseif (is_array($rate)) {
            if (empty($rate[$currency])) {
                throw new InvalidCurrencyException();
            } elseif (is_numeric($rate[$currency])) {
                return $amount * $rate[$currency];
            } elseif (is_string($rate[$currency])) {
                return (new Helpers())->currencyRate($rate[$currency], $amount, $currency);
            }
        } elseif (is_string($rate)) {
            return (new Helpers())->currencyRate($rate[$currency], $amount, $currency);
        } else {
            throw new InvalidServiceRateException("service must implements interface " . RateServiceInterface::class);
        }
    }

    public function balance()
    {
        return $this->balance;
    }

    public function lockedBalance()
    {
        return $this->locked_balance;
    }

    public function blocked()
    {
        return $this->locked_balance;
    }

    public function transactions(string $type = null)
    {
        $query = SimpleWalletTransaction::where('payer_wallet_id', $this->id);
        if (!empty($type)) {
            $query = $query->where('type', $type);
        }

        return $query->orderBy('id', 'desc');
    }
}
