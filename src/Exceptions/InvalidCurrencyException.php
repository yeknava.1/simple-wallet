<?php

namespace Yeknava\SimpleWallet\Exceptions;

class InvalidCurrencyException extends SimpleWalletException {
}