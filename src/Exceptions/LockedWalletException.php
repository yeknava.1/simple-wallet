<?php

namespace Yeknava\SimpleWallet\Exceptions;

class LockedWalletException extends SimpleWalletException {
}