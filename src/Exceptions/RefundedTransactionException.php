<?php

namespace Yeknava\SimpleWallet\Exceptions;

class RefundedTransactionException extends SimpleWalletException {
}