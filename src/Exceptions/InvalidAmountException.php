<?php

namespace Yeknava\SimpleWallet\Exceptions;

class InvalidAmountException extends SimpleWalletException {
}