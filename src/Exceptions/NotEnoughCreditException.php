<?php

namespace Yeknava\SimpleWallet\Exceptions;

class NotEnoughCreditException extends SimpleWalletException {
}