<?php

namespace Yeknava\SimpleWallet\Exceptions;

class PayeeNotFoundException extends SimpleWalletException {
}