<?php

namespace Yeknava\SimpleWallet\Exceptions;

class WalletNotFoundException extends SimpleWalletException {
}