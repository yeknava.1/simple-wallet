<?php

namespace Yeknava\SimpleWallet\Exceptions;

class InvalidTransactionException extends SimpleWalletException {
}