<?php

namespace Yeknava\SimpleWallet\Exceptions;

class PayerNotFoundException extends SimpleWalletException {
}