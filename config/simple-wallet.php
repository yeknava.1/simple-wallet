<?php

use Yeknava\SimpleWallet\Samples\UsdRateServiceSample;

return [
    'default_currency' => 'irt',

    'currencies' => [
        'irr' => 'IRR',
        'irt' => 'IRT',
        'usd' => 'USD',
    ],

    'rates' => [
        'irr' => [
            'irt' => 0.1,
            'usd' => 0.0067
        ],
        'irt' => [
            'irr' => 10,
            'usd' => 0.067
        ],
        'usd' => UsdRateServiceSample::class
    ],

    'wallet_default_slug' => 'wallet',

    // if true, wallet will be created if holder has not any wallet
    // useful when app does not have this package from first day of it's launch
    'create_wallet_on_fetch' => true,

    'wallets_table' => 'wallets',

    'transactions_table' => 'wallet_transactions',

    'wallet_logs_table' => 'wallet_logs'
];
