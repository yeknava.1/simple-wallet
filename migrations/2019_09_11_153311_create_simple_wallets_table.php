<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-wallet.wallets_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->nullableMorphs('holder');
            $table->string('name')->nullable();
            $table->string('slug')->index();
            $table->double('balance', 14, 4)->default(0);
            $table->double('locked_balance', 14, 4)->unsigned()->default(0);
            $table->boolean('is_default')->default(true);
            $table->tinyInteger('locked')->default(0);
            $table->timestamp('locked_at')->nullable();
            $table->string('currency')->default(config('simple-wallet.default_currency'));
            $table->unique(['holder_id', 'holder_type', 'slug']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-wallet.wallet_table'));
    }
}
