<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-wallet.transactions_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payer_wallet_id')->unsigned()->nullable()->index();
            $table->foreign('payer_wallet_id')->references('id')->on(config('simple-wallet.wallets_table'));
            $table->bigInteger('payee_wallet_id')->unsigned()->nullable()->index();
            $table->foreign('payee_wallet_id')->references('id')->on(config('simple-wallet.wallets_table'));
            $table->double('amount', 14, 4)->unsigned()->default(0);
            $table->double('fee', 14, 4)->unsigned()->default(0);
            $table->string('type')->nullable();
            $table->nullableMorphs('service');
            $table->string('currency')->default(config('simple-wallet.default_currency'));
            $table->jsonb('extra')->nullable();
            $table->timestamp('refunded_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-wallet.transactions_table'));
    }
}
