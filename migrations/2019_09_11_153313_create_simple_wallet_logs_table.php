<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleWalletLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('simple-wallet.wallet_logs_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('wallet_id')->unsigned()->nullable()->index();
            $table->foreign('wallet_id')->references('id')->on(config('simple-wallet.wallets_table'));
            $table->bigInteger('wallet_transaction_id')->unsigned()->nullable()->index();
            $table->foreign('wallet_transaction_id')->references('id')->on(config('simple-wallet.transactions_table'));
            $table->string('type');
            $table->double('amount', 14, 4)->default(0);
            $table->double('balance', 14, 4)->default(0);
            $table->double('new_balance', 14, 4)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('simple-wallet.wallet_logs_table'));
    }
}
