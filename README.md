# Laravel Simple Wallet Package

Laravel Simple Wallet Package. this package heavily inspired by [laravel-wallet](https://github.com/bavix/laravel-wallet) package. if you need a full feature wallet you should use laravel-wallet. i made this package because i needed a simple wallet functionality with some modifications which laravel-wallet didn't support them at that time, like cash back feature or supporting a system (core) wallet which has sum of all purchases. also you may lock wallet balance or part of it or even lock whole wallet and release it after.

## Features

-	Deposit
-	Withdraw
-	Gift (user to user)
-	System Gift (system to user)
-	Purchase
-   Cashback
-	Refund
-	Lock Balance
-	Lock / Full Lock Wallet
-	Transactions history
-	Multi wallets
-	Currency exchange
-	Force transaction (negative balance)

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple wallet package.

```bash
composer require yeknava/simple-wallet
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

Add HasWallet trait to models that have wallets.

```php
<?php

use Yeknava\SimpleWallet\HasWallet;

class User extends Model {
    use HasWallet;

    protected static function boot()
    {
        parent::boot();
        self::walletBoot();
    }
}
```

```php
$user = User::first();
$user->balance(); // float(0.0)

$user->deposit(100);
$user->balance(); // float(100.0)

$user->withdraw(10);
$user->balance(); // float(90.0)

$system = User::where('type', 'system')->first();

$user->withdraw(10, ['desc'=>'monthly_fee'], null, null, $system->wallet());
$user->balance(); // float(80.0)
$system->balance(); // float(10.0)

$user2 = User::last();
$user2->balance(); // float(0.0)
$user->transfer(10, $user2->wallet());
$user->balance(); // float(70.0)
$user2->balance(); // float(10.0)
$user2->forceTransfer(20, $user->wallet());
$user2->balance(); // float(-10.0)
$user->balance(); // float(90.0)

$user->lockBalance(10);
$user->balance(); // float(80.0)
$user->blocked(); // float(10.0)
$user->releaseBalance(5); // float(10.0)
$user->balance(); // float(85.0)
$user->blocked(); // float(5.0)

$user->lockWallet(); // lock for pay but available for being paid 
$user->transfer(10, $user2->wallet()); // error
$user->deposit(10); // success
$user->fullLockWallet(); // lock for pay or being paid
$user->deposit(10); // error
$user->releaseWallet();
$user->transfer(10, $user2->wallet()); // success

// purchase an item with 50 price which user2 is owner of it and with 5 cashback for payer
$user->balance(); // float(85.0)
$user2->balance(); // float(0.0)
$user->purchase(50, "sample_service", $user2->wallet(), null, ['fee'=>2], 5); 
$user->balance(); // float(40.0)
$user2->balance(); // float(45.0)

$payment = $user1->transactions()->where('service_id', 'sample_service')->first();
$user1->refund($payment);
$user->balance(); // float(85.0)
$user2->balance(); // float(0.0)

$user1NewWallet = $user1->createWallet([], 'wallet2'); //new wallet with 'wallet2' slug
$user1NewWallet->balance(); // float(0.0)
$user->balance(); // float(85.0)
$user1NewWallet->makeDefault();
$user->balance(); // float(0.0)
```
## Config
```php
<?php

use Yeknava\SimpleWallet\Samples\UsdRateServiceSample;

return [
    'default_currency' => 'irt',

    'currencies' => [
        'irr' => 'IRR',
        'irt' => 'IRT',
        'usd' => 'USD',
    ],

    'rates' => [
        'irr' => [
            'irt' => 0.1,
            'usd' => 0.0067
        ],
        'irt' => [
            'irr' => 10,
            'usd' => 0.067
        ],
        'usd' => UsdRateServiceSample::class
    ],

    'wallet_default_slug' => 'wallet',

    // if true, wallet will be created on calling any wallet
    // specific methods in case holder has not any wallet
    'create_wallet_on_fetch' => true 
];
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
