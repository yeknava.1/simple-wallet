<?php

use Orchestra\Testbench\TestCase;
use Tests\ServiceModel;
use Tests\UserModel;
use Yeknava\SimpleWallet\Exceptions\InvalidTransactionException;
use Yeknava\SimpleWallet\Exceptions\LockedWalletException;
use Yeknava\SimpleWallet\Exceptions\NotEnoughCreditException;
use Yeknava\SimpleWallet\Exceptions\RefundedTransactionException;
use Yeknava\SimpleWallet\Exceptions\SimpleWalletException;
use Yeknava\SimpleWallet\SimpleWalletLog;

class SimpleTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp() : void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('simple-wallet', require_once(__DIR__ . '/../config/simple-wallet.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleWallet\SimpleWalletServiceProvider::class,
        ];
    }

    public function test()
    {        
        $user1 = (new UserModel([]));
        $user1->save();
        $user2 = (new UserModel([]));
        $user2->save();
        $service1 = (new ServiceModel([]));
        $service1->save();

        $this->assertEquals(0, $user1->balance());
        $this->assertEquals(0, $user2->balance());

        //deposit
        $user1->deposit(110, ['service' => $service1]);

        $this->assertEquals(110, $user1->balance());
        $log = $user1->wallets()->first()->logs()->first();
        $this->assertEquals(110, $log->new_balance);
        $this->assertEquals(0, $log->balance);
        $this->assertEquals(110, $log->amount);
        $this->assertEquals(SimpleWalletLog::TYPE_DEPOSIT, $log->type);

        // transfer
        $user1->transfer(50, $user2->wallet(), null, ['fee' => 10]);
        $this->assertEquals(50, $user1->balance());
        $this->assertEquals(50, $user2->balance());

        try {
            $user1->transfer(60, $user2->wallet());
        } catch (Throwable $t) {
            $this->assertInstanceOf(NotEnoughCreditException::class, $t);
            $this->assertInstanceOf(SimpleWalletException::class, $t);
            $this->assertNotInstanceOf(InvalidTransactionException::class, $t);
        }

        $user1->forceTransfer(60, $user2->wallet());
        $this->assertEquals(-10, $user1->balance());
        $this->assertEquals(110, $user2->balance());

        // withdraw
        $user2->withdraw(10);
        $this->assertEquals(100, $user2->balance());

        try {
            $user2->withdraw(110);
        } catch (Throwable $t) {
            $this->assertInstanceOf(NotEnoughCreditException::class, $t);
            $this->assertInstanceOf(SimpleWalletException::class, $t);
            $this->assertNotInstanceOf(InvalidTransactionException::class, $t);
        }

        $user2->forceWithdraw(110);
        $this->assertEquals(-10, $user2->balance());

        // gift
        $user2->systemGift(220);
        $user1->gift(110, $user2->wallet());
        $this->assertEquals(100, $user1->balance());
        $this->assertEquals(100, $user2->balance());

        // purchase
        $user1->purchase(10, $service1);
        $user1->purchase(10, $service1, $user2->wallet());
        $this->assertEquals(80, $user1->balance());
        $this->assertEquals(110, $user2->balance());

        try {
            $user1->purchase(110, $service1);
        } catch (Throwable $t) {
            $this->assertInstanceOf(NotEnoughCreditException::class, $t);
            $this->assertInstanceOf(SimpleWalletException::class, $t);
            $this->assertNotInstanceOf(InvalidTransactionException::class, $t);
        }

        $this->assertEquals(80, $user1->balance());
        $user1->forcePurchase(110, $service1);
        $this->assertEquals(-30, $user1->balance());

        $payment1 = $user1->servicePurchases($service1)->latest('id')->first();
        $user1->refund($payment1);
        $this->assertEquals(80, $user1->balance());

        $payment = $user1->servicePurchases($service1)->latest('id')->first();
        try {
            $user1->refund($payment);
        } catch (Throwable $t) {
            $this->assertInstanceOf(RefundedTransactionException::class, $t);
        }
        $this->assertEquals(80, $user1->balance());
        $this->assertEquals(110, $user2->balance());

        try {
            $user1->lockBalance(90);
        } catch (Throwable $t) {
            $this->assertInstanceOf(NotEnoughCreditException::class, $t);
        }

        $this->assertEquals(80, $user1->balance());
        $user1->deposit(10);
        $user1->lockBalance(90);
        try {
            $user1->purchase(90, $service1);
        } catch (Throwable $t) {
            $this->assertInstanceOf(NotEnoughCreditException::class, $t);
            $this->assertInstanceOf(SimpleWalletException::class, $t);
            $this->assertNotInstanceOf(InvalidTransactionException::class, $t);
        }
        $this->assertEquals(0, $user1->balance());
        $user1->forcePurchase(90, $service1);
        $this->assertEquals(-90, $user1->balance());
        $this->assertEquals(90, $user1->blocked());
        $user1->releaseBalance();
        $this->assertEquals(0, $user1->balance());

        // lock
        $user1->lockWallet();
        try {
            $user1->withdraw(100);
        } catch (Throwable $t) {
            $this->assertInstanceOf(LockedWalletException::class, $t);
            $this->assertInstanceOf(SimpleWalletException::class, $t);
            $this->assertNotInstanceOf(InvalidTransactionException::class, $t);
        }

        $user1->deposit(110);
        $this->assertEquals(110, $user1->balance());

        $user1->fullLockWallet();
        try {
            $user1->deposit(100);
        } catch (Throwable $t) {
            $this->assertInstanceOf(LockedWalletException::class, $t);
            $this->assertInstanceOf(SimpleWalletException::class, $t);
            $this->assertNotInstanceOf(InvalidTransactionException::class, $t);
        }

        $user1->releaseWallet();
        $user1->deposit(90);
        $this->assertEquals(200, $user1->balance());

        // multi wallet
        $user1NewWallet = $user1->createWallet([], 'new_wallet');
        $this->assertEquals(0, $user1NewWallet->balance());
        $this->assertEquals(200, $user1->balance());
        $user1NewWallet->makeDefault();
        $this->assertEquals(0, $user1->balance());
    }
}