<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Yeknava\SimpleWallet\HasWallet;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model
{
    use HasWallet, SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        self::walletBoot();
    }

    protected $table = 'simple_wallet_test';
}
